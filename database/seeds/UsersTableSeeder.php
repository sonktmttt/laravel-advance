<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('users')->truncate();

        Schema::enableForeignKeyConstraints();

        $users = [
            ['sonnt1991', 'sonnt1991@gmail.com', '123456', 1],
            ['a user', 'a@gmail.com', '123456', 2],
            ['b user', 'b@gmail.com', '123456', 2],
        ];

        foreach ($users as $user) {
            $created = App\User::create([
                'name' => $user[0],
                'email' => $user[1],
                'password' => bcrypt($user[2]),
                'role' => $user[3],
            ]);
        }
    }
}
